'- Indianópolis , ZX-81 , Microcomputador Jogos , Página 73
'- status não informado pelo Digitador
'- Digitador desconhecido

10 REM
20 REM      INDIANAPOLIS
30 REM  PARA A LINHA SINCLAIR
40 REM
50 CLS
60 SLOW
70 LET E=PEEK 16396+256*PEEK 16397
80 LET A=E+314
90 LET B=13
100 LET C=0
110 FOR X=0 TO 21
120 PRINT AT X,0;"\##\##\##\##\##\##\##\##\##\##\##\##\##\:     \ :\##\##\##\##\##\##\##\##\##\##\##\##\##"
130 NEXT X
140 REM  ACAO CENTRAL
150 LET L$=INKEY$
160 POKE A,11
170 IF L$="5" THEN LET A=A-1
180 IF L$="8" THEN LET A=A+1
190 SCROLL
200 IF PEEK A<>0 THEN GOTO 290
210 POKE A,59
220 LET C=C+1
230 REM    MONTAGEM PISTA
240 LET D=INT (RND*11)
250 LET B=B+(D>5 AND B<23)-(D<4 AND B>1)
260 PRINT AT 21,0;"\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##\##";
270 PRINT AT 21,B;"\:     \ :"
280 GOTO 150
290 REM    BATIDA
300 POKE A,151
310 PRINT AT 1,1;C;" KMS PERCORRIDOS"
320 PRINT AT 3,0;"DESEJA PILOTAR NOVAMENTE (S/N) ?"
330 IF INKEY$="S" THEN RUN
340 IF INKEY$<>"N" THEN GOTO 330


