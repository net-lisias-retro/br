'- 2.2 Pseudo-borda na screen 0 , MSX , 100 Dicas para MSX , Página 23
'- Não testado
'- <Lisias Toledo> me@lisias.net

10 COLOR 1,15 : SCREEN 0
20 FOR F=2048 TO 4095
30   X = NOT(VPEEK(F)) AND 255
40   VPOKE F, X
50 NEXT F
60 INPUT "Qual a cor da borda (0-15)";B
70 IF B<0 OR B>15 THEN 60
80 COLOR ,B
90 GOTO 60
