'- 7.J Pseudo-ramdisk , MSX , 100 Dicas para MSX , Página 181
'- Revisado por <Lisias Toledo> me@lisias.net
'- Digitador desconhecido

100 SCREEN 0:WIDTH 39:CLEAR 200,&HD000
110 FOR F=&HD000 TO &HD076
120   READ A$:POKE F,VAL("&H"+A$)
130 NEXT F
140 DEFUSR0=&HD000
150 DATA F3,F5,C5,D5,E5,ED,73,FE
160 DATA F3,FE,02,20,5E,23,23,7E
170 DATA FE,00,20,0B,21,00,80,11
180 DATA 00,00,CD,53,D0,18,43,FE
190 DATA 01,20,0B,21,00,00,11,00
200 DATA 80,CD,53,D0,18,34,FE,02
210 DATA 20,39,CD,53,D0,01,FF,5F
220 DATA 11,00,00,21,00,80,1A,32
230 DATA 75,D0,7E,12,3A,75,D0,77
240 DATA 23,13,0B,78,B1,20,EF,DB
250 DATA A8,18,14,DB,A8,47,CB,3F
260 DATA CB,3F,CB,3F,CB,3F,80,D3
270 DATA A8,C9,01,FF,5F,ED,B0,E6
280 DATA F0,D3,A8,ED,7B,FE,F3,E1
290 DATA D1,C1,F1,FB,C9,00,00,00

