'- 6.4 Mapa de discos , MSX , 100 Dicas para MSX , P�gina 141
'- Revisado por <Lisias Toledo> me@lisias.net
'- Digitador desconhecido

4000 REM
4010 REM mapa do disquete
4020 REM -----------------------------
4030 PLAY "S0M5000O3E#"
4040 SCREEN 0 : WIDTH 36 : KEY OFF
4050 PRINT SPC(6);">>> MAPA DO DISQUETE <<<"
4060 PRINT SPC(6);STRING$(24,"-")
4070 PRINT:PRINT:PRINT
4080 PRINT SPC(6);" >>> LENDO A F.A.T. <<<":LOCATE0,3,0
4090 GOSUB 8260
4100 FOR F=5 TO 11
4110   LOCATE0,2,0:PRINTCHR$(27)"J"
4120   PRINTSPC(9);">>> SETOR";
4130   PRINT USING "###";F;
4140   PRINT " <<<"
4150   PRINTSPC(9);"================"
4160   A$ = DSKI$(0,F)
4170   P = PEEK(&HF351)+256*PEEK(&HF352)
4180   FOR G=0 TO 15
4190   LOCATE0,5,0:PRINTCHR$(27)"J";
4200   IF INKEY$=CHR$(27) THEN 1000
4210     PRINT "ARQUIVO";(F-5)*16+G+1
4220     PRINT "-----------"
4230 REM --------------------------
4240     PRINT"NOME       : ";
4250     C$ = ""
4260     FOR H=0 TO 10
4270       A = P + 32*G + H
4280       IF PEEK (A)<>0 THEN 4310
4290       PRINTSTRING$(95,127)
4300        GOTO 4920
4310       C$ = C$ + CHR$(PEEK(A))
4320       IF H=7 THEN C$=C$+"."
4330     NEXT H
4340     PRINT C$
4350 REM --------------------------
4360     PRINT"ATRIBUTOS  :";
4370     AT = PEEK ( P + 32*G + 11 )
4380     AT$ = RIGHT$("00000000"+BIN$(AT),8)
4390     PRINT " ";AT$
4400 REM --------------------------
4410     PRINT"HORA       :";
4420     X=(P+32*G+22)
4430     X1=PEEK(X)
4440     X2=PEEK(X+1)
4450     X1$=RIGHT$("00000000"+BIN$(X1),8)
4460     X2$=RIGHT$("00000000"+BIN$(X2),8)
4470     H$=LEFT$(X2$,5)
4480     M$=RIGHT$(X2$,3)+LEFT$(X1$,3)
4490     H=VAL("&B"+H$)
4500     M=VAL("&B"+M$)
4510     PRINT " ";H;M
4520 REM --------------------------
4530     PRINT"DATA       :";
4540     H2=PEEK(P + 32*G + 24)
4550     H1=PEEK (P + 32*G + 25)
4560     H1$=RIGHT$("00000000"+BIN$(H1),8)
4570     H2$=RIGHT$("00000000"+BIN$(H2),8)
4580     D=VAL("&B"+RIGHT$(H2$,5))
4590     M=VAL("&B"+RIGHT$(H1$,1)+LEFT$(H2$,3))
4600     A=1980+VAL("&B"+LEFT$(H1$,7))
4610     PRINT D;M;A
4620 REM --------------------------
4630     PRINT"1� BLOCO   :  ";
4640     H=(P+32*G+26)
4650     H=PEEK(H)+256*PEEK(H+1)
4660     PB=H
4670     PRINT RIGHT$("000"+HEX$(H),3)
4680 REM --------------------------
4690     PRINT"N� DE BYTES: ";
4700     H=(P+32*G+28)
4710     H=PEEK(H)+256*PEEK(H+1)+4096*PEEK(H+2)+65536!*PEEK(H+3)
4720     PRINT RIGHT$("0000"+HEX$(H),4)
4730 REM --------------------------
4740     PRINT"MAPA DA FAT: ";
4750     H=PB
4760     IF H>359 THEN 4830
4770     PRINT RIGHT$("000"+HEX$(B%(H)),3);" ";
4780     IF H>359 THEN 4830
4790     H=B%(H)
4800     IF H>359 THEN 4830
4810     PRINT RIGHT$("000"+HEX$(B%(H)),3);" ";
4820     GOTO 4780
4830 REM --------------------------
4840     PRINT
4850     PRINT
4860     IF LEFT$(C$,1)=CHR$(&HE5) THEN GOSUB 8070
4870   LOCATE10,23,0
4880   PRINT"DIGITE RETURN:";
4890   A$=INPUT$(1)
4900   NEXT G
4910 NEXT F
4920 LOCATE8,23,0
4930 PRINT ">>> TECLE ESPA�O <<<";
4940 IF STRIG(0)=0 THEN 4940 ELSE RUN
8070 REM
8080 REM Recupera deletados
8090 REM -----------------------------
8100 PRINT,,,"> Arquivo deletado ! <"
8110 PRINT,"RECUPERAR (S/N)?:";
8120 BEEP : BEEP : BEEP
8130 X = PEEK(&HFCAB) : POKE &HFCAB,1
8140 X$ = INPUT$(1) : PRINT X$
8150 IF X$<>"S" THEN 8220
8160   PRINT,"1� caractere do nome:";
8170   Z$ = INPUT$(1) : PRINT Z$
8180   IF Z$<"A" OR Z$>"Z" THEN 8160
8190   POKE (A-10),ASC(Z$)
8200   DSKO$ 0,F
8210   GOTO 8230
8220 IF X$<>"N" THEN 8110
8230 POKE &HFCAB,X : PRINT : PRINT
8240 RETURN
8250 REM
8260 REM leitor de F.A.T.
8270 REM -----------------------------
8280 A%(0)=B%(0):ERASE A%,B%
8290 DIM A%(539),B%(359)
8300 A$=DSKI$(0,1)
8310 P = PEEK(&HF351)+256*PEEK(&HF352)
8320 FOR F=0 TO 511
8330   A%(F) = PEEK(P+F)
8340 NEXT F
8350 A$=DSKI$(1,2)
8360 P = PEEK(&HF351)+256*PEEK(&HF352)
8370 FOR F=0 TO 27
8380   A%(F+512) = PEEK(P+F)
8390 NEXT F
8400 G=0
8410 FOR F=0 TO 539 STEP 3
8420  IF INKEY$=CHR$(27) THEN 1000
8430  B%(G)  =A%(F)+256*(A%(F+1) AND &HF)
8440  B%(G+1)=(A%(F+1) AND &HF0)/16 + A%(F+2)*16
8450  G=G+2
8460 NEXT F
8470 FOR F=1 TO 360
8480  IF INKEY$=CHR$(27) THEN 1000
8490   PRINT RIGHT$("000"+HEX$(F-1),3);"W";RIGHT$("000"+HEX$(B%(F-1)),3);" ";
8500   IF F/72 <> F\72 THEN 8550
8510    LOCATE 10,23,1
8520    PRINT "DIGITE RETURN:";
8530    A$=INPUT$(1)
8540    LOCATE 0,3,0
8550 NEXT F
8560 PRINT CHR$(27);"J"
8570 RETURN
