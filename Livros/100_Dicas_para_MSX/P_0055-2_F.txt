'- 2.F Scroll right para a screen 0, MSX , 100 Dicas para MSX , Página 55
'- Revisado por <Lisias Toledo> me@lisias.net
'- Digitador desconhecido

100 REM -------------------------------
110 REM SCROLL SCREEN 0 RIGHT-Rubens Jr
120 REM -------------------------------
130 FOR F=&HE000 TO &HE03F
140   READ A$ : POKE F,VAL("&H"+A$)
150 NEXT F : DEFUSR0=&HE000
160 DATA 21,00,00,22,3D,E0,06,18
170 DATA F3,C5,CD,1C,E0,C1,2A,3D
180 DATA E0,11,28,00,19,22,3D,E0
190 DATA 10,EF,FB,C9,E5,01,28,00
200 DATA C5,11,18,FC,D5,CD,59,00
210 DATA 21,3E,FC,11,3F,FC,01,27
220 DATA 00,ED,B8,3E,20,12,E1,C1
230 DATA D1,CD,5C,00,C9,00,00,E1
240 REM -------------------------------
250 REM         Exemplo de uso
260 REM -------------------------------
270 SCREEN 0 : WIDTH 40 : KEY OFF
280 X = 23 * RND(1)
290 LOCATE 0,X : PRINT ".";
300 X = USR0(0)
310 GOTO 280

