'- 7.D Trocando tokens num programa , MSX , 100 Dicas para MSX , Página 169
'- Revisado por <Lisias Toledo> me@lisias.net
'- Digitador desconhecido

50000 '*********************
50010 '* BY THE DOCTOR LUZ *
50020 '*********************
50030 FOR L=&HC000 TO &HC09F
50040 READ A$:POKE L,VAL("&H"+A$)
50050 NEXT
50060 DEF USR=&HC000
50070 PRINT"RODAR PROGRAMA ?"
50080 A$=INPUT$(1)
50090 IF A$="S" THEN GOTO 50100 ELSE END
50100 INPUT"CODIGO DA TOKEN";A
50110 INPUT"NOVO CODIGO";B
50120 POKE &HC09C,A
50130 POKE &HC09B,B
50140 A=USR(0)
50150 LIST
50160 DATA DD,21,FF,7F,DD,23,21,73
50170 DATA C0,DD,7E,00,06,14,BE,28
50180 DATA 21,23,23,10,F9,DD,7E,00
50190 DATA FE,22,28,39,5F,3A,9C,C0
50200 DATA DD,BE,00,20,DF,3A,9B,C0
50210 DATA DD,77,00,7B,FE,84,28,36
50220 DATA 18,D2,DD,7E,00,FE,00,20
50230 DATA 0E,DD,7E,01,FE,00,20,07
50240 DATA DD,7E,02,FE,00,28,09,23
50250 DATA 7E,47,DD,23,10,FC,18,B4
50260 DATA DD,22,9D,C0,C9,DD,23,DD
50270 DATA 7E,00,FE,16,28,A6,FE,00
50280 DATA 20,F3,DD,2B,18,9E,DD,23
50290 DATA DD,7E,00,FE,00,20,F7,DD
50300 DATA 2B,18,91,00,04,0B,03,0C
50310 DATA 03,0D,03,0E,03,0F,02,11
50320 DATA 01,12,01,13,01,14,01,15
50330 DATA 01,16,01,17,01,18,01,19
50340 DATA 01,1A,01,1C,03,1D,04,1F
50350 DATA 08,FF,02,00,00,00,00,20

